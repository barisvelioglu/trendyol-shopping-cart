using System;

namespace ECommerce.ShoppingCart.Exceptions
{
  public class NotFoundException : Exception
  {
    public NotFoundException(string message) : base(message)
    {

    }
  }
}