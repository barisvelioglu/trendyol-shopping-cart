using System;

namespace ECommerce.ShoppingCart.Exceptions
{
  public class CouponAlreadyInUseException : Exception
  {
    public CouponAlreadyInUseException(string message) : base(message)
    {
    }
  }
}