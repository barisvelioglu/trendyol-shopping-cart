using System;
using ECommerce.ShoppingCart;

public class InCategoryAndQuantitySufficientRule : ICategoryRule, IQuantityRule
{
  public Guid CategoryId { get; set; }
  public string Description { get; set; }
  public int Quantity { get; set; }

  public InCategoryAndQuantitySufficientRule(string description, Guid categoryId, int quantity)
  {
    if (categoryId == null || categoryId == Guid.Empty)
      throw new ArgumentNullException(nameof(categoryId));

    if (quantity < 1)
      throw new ArgumentOutOfRangeException(nameof(quantity));

    Description = description;
    Quantity = quantity;
    CategoryId = categoryId;
  }

  public bool IsMatched(CartModel model)
  {
    var currentQuantity = 0;

    foreach (var item in model.GetItems())
    {
      if (CategoryId == item.CategoryId)
      {
        currentQuantity += item.Quantity;
      }
    }

    if (currentQuantity >= Quantity)
      return true;

    return false;
  }
}



