public interface IProfileRule : IRule
{
  string Gender { get; set; }
  int? Age { get; set; }
}
