using System;
using System.Collections.Generic;
using ECommerce.ShoppingCart;

public interface ICategoryRule : IRule
{
  Guid CategoryId { get; set; }
}
