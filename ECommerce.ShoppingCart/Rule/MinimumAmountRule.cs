using System;

public class MinimumAmountRule : IMinimumAmountRule
{
  public decimal MinimumAmount { get; set; }
  public string Description { get; set; }

  public MinimumAmountRule(string description, decimal minimumAmount)
  {
    if (minimumAmount <= 0)
      throw new ArgumentOutOfRangeException(nameof(minimumAmount));

    MinimumAmount = minimumAmount;
    Description = description;
  }

  public bool IsMatched(CartModel model)
  {
    decimal totalAmount = 0;
    foreach (var item in model.GetItems())
    {
      totalAmount += item.UnitPrice.Amount * item.Quantity;
    }

    return totalAmount >= MinimumAmount;
  }
}



