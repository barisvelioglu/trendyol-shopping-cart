public interface IRule
{
  string Description { get; set; }
  bool IsMatched(CartModel model);
}
