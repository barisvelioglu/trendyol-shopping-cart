public interface IMinimumAmountRule : IRule
{
  decimal MinimumAmount { get; set; }
}
