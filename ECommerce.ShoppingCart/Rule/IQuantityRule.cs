public interface IQuantityRule : IRule
{
  int Quantity { get; set; }
}
