using System;

public class RateDiscountCalculator : IDiscountCalculator
{
  public decimal Calculate(decimal totalAmount, decimal discountAmount)
  {
    if (discountAmount > 100 || discountAmount < 1)
      throw new ArgumentOutOfRangeException("Rate cannot be greater than 100 or less than 1!");

    var totalDiscount = (totalAmount * discountAmount / 100);

    if (totalDiscount > totalAmount)
      return totalAmount;

    return totalDiscount;
  }
}



