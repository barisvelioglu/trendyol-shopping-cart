using System;
using System.Collections.Generic;
using ECommerce.ShoppingCart.Exceptions;

public class Coupon : BaseDiscount, IDiscount
{
  private readonly string _code;
  protected IDiscountCalculator _discountCalculator;
  public decimal DiscountAmount { get; private set; }
  public string Description { get; private set; }
  public bool IsUsed { get; private set; }

  public Coupon(string code, decimal discountAmount, DiscountType discountType, string description)
        : base(null)
  {
    if (string.IsNullOrEmpty(code))
      throw new ArgumentNullException(nameof(code));

    if (discountAmount <= 0)
      throw new ArgumentOutOfRangeException(nameof(discountAmount));

    _code = code;
    _discountCalculator = DiscountCalculatorFactory.GetCalculator(discountType);

    DiscountAmount = discountAmount;
    Description = description;
    IsUsed = false;
  }

  public Coupon(IList<IRule> rules, string code, decimal discountAmount, DiscountType discountType, string description)
         : this(code, discountAmount, discountType, description)
  {
    _rules = rules;
  }

  public bool IsApplicable(CartModel model)
  {
    return CheckRules(model);
  }
  public decimal CalculateDiscount(CartModel model)
  {
    if (CheckRules(model))
      return _discountCalculator.Calculate(model.GetTotalAmountAfterDiscounts(), DiscountAmount);

    return 0;
  }
  public void ApplyCoupon()
  {
    if (IsUsed)
      throw new CouponAlreadyInUseException("Coupon is already in use!");

    IsUsed = true;
  }
}



