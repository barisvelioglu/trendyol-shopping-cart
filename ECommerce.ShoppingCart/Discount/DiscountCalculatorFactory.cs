using System;

public static class DiscountCalculatorFactory
{
  public static IDiscountCalculator GetCalculator(DiscountType discountType)
  {
    switch (discountType)
    {
      case DiscountType.Amount:
        return new AmountDiscountCalculator();
      case DiscountType.Rate:
        return new RateDiscountCalculator();
      default:
        throw new NotImplementedException();
    }
  }
}



