using System;
using System.Collections.Generic;
using System.Linq;

public class Campaign : BaseDiscount, IDiscount
{
  private readonly ICategoryRule _rule;
  protected IDiscountCalculator _discountCalculator;
  public readonly decimal DiscountAmount;
  public readonly string Description;

  public Campaign(ICategoryRule rule, decimal discountAmount, DiscountType discountType, string description) : base(new List<IRule>() { rule })
  {
    if (rule == null)
      throw new ArgumentNullException(nameof(rule));

    if (discountAmount <= 0)
      throw new ArgumentOutOfRangeException(nameof(discountAmount));

    _rule = rule;
    _discountCalculator = DiscountCalculatorFactory.GetCalculator(discountType);
    DiscountAmount = discountAmount;
    Description = description;
  }

  public Guid GetCategoryId()
  {
    return _rule.CategoryId;
  }
  public bool IsApplicable(CartModel model)
  {
    return CheckRules(model);
  }
  public decimal CalculateDiscount(CartModel model)
  {
    if (CheckRules(model))
    {
      var relatedCategoryItems = model.GetItems().ToList().Where(x => x.CategoryId == _rule.CategoryId);
      decimal relatedCategoryItemsTotalAmount = 0;

      foreach (var item in relatedCategoryItems)
      {
        relatedCategoryItemsTotalAmount += item.UnitPrice.Amount * item.Quantity;
      }

      return _discountCalculator.Calculate(relatedCategoryItemsTotalAmount, DiscountAmount);
    }

    return 0;
  }

}



