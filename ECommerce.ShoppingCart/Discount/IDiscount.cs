public interface IDiscount
{
  bool IsApplicable(CartModel model);
  decimal CalculateDiscount(CartModel model);
}



