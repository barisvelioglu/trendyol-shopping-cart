public interface IDiscountCalculator
{
  decimal Calculate(decimal totalAmount, decimal discountAmount);
}



