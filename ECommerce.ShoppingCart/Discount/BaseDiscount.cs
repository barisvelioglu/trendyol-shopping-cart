﻿using System.Collections.Generic;

public abstract class BaseDiscount
{
  protected IList<IRule> _rules;
  public BaseDiscount(IList<IRule> rules)
  {
    _rules = rules;
  }
  protected bool CheckRules(CartModel model)
  {
    if (_rules == null)
      return true;

    foreach (var rule in _rules)
    {
      if (!rule.IsMatched(model))
        return false;
    }

    return true;
  }
}




