using System;

public class AmountDiscountCalculator : IDiscountCalculator
{
  public decimal Calculate(decimal totalAmount, decimal discountAmount)
  {
    if (discountAmount < 0)
      throw new ArgumentException("Discount cannot be negative!");

    if (discountAmount > totalAmount)
      return totalAmount;

    return discountAmount;
  }
}



