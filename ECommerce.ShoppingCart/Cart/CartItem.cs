using System;
using ECommerce.Shared;

public class CartItem
{
  public Guid ProductId { get; set; }
  public string ProductName { get; set; }
  public Guid CategoryId { get; set; }
  public string CategoryName { get; set; }
  public int Quantity { get; set; }
  public Money UnitPrice { get; set; }
}

