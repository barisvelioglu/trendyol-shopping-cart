public class CartDiscount
{
  public string Description { get; set; }
  public decimal Amount { get; set; }
  public DiscountMethod DiscountMethod { get; set; }
}

