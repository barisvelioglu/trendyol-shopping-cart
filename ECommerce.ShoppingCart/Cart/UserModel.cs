using System;

public class UserModel
{
  public string Gender { get; set; }
  public DateTime? BirthDate { get; set; }
}