using System;
using System.Collections.Generic;
using ECommerce.Shared;

namespace ECommerce.ShoppingCart
{
  public interface IShoppingCart
  {
    IList<CartDiscount> GetDiscounts();
    IList<CartItem> GetItems();
    void AddItem(Guid productId, string productName, Guid categoryId, string categoryName, Money unitPrice, int quantity);
    void RemoveItem(Guid productId, int quantity);

    void ApplyCampaigns(params Campaign[] campaigns);
    void ApplyCoupon(Coupon coupon);
    decimal GetCampaignDiscount();
    decimal GetCouponDiscount();
    decimal GetTotalDiscounts();
    decimal GetDeliveryCost();
    decimal GetTotalAmount();
    decimal GetTotalAmountAfterDiscounts();
    void SetBuyerId(string buyerId);
    string ToString();
  }

}
