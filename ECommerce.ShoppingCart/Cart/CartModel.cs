using System;
using System.Collections.Generic;

public class CartModel
{
  private IList<CartItem> Items;
  private IList<CartDiscount> Discounts;

  public CartModel(IList<CartItem> items, IList<CartDiscount> discounts)
  {
    if (items == null)
      throw new ArgumentNullException(nameof(items));

    if (discounts == null)
      throw new ArgumentNullException(nameof(items));

    Items = items;
    Discounts = discounts;
  }

  public decimal GetTotalAmount()
  {
    decimal totalAmount = 0;
    foreach (var item in Items)
    {
      totalAmount += item.UnitPrice.Amount * item.Quantity;
    }

    return totalAmount;
  }

  public decimal GetTotalDiscounts()
  {
    decimal totalDiscount = 0;
    foreach (var discount in Discounts)
    {
      totalDiscount += discount.Amount;
    }

    return totalDiscount;
  }

  public decimal GetTotalAmountAfterDiscounts()
  {
    return GetTotalAmount() - GetTotalDiscounts();
  }

  public IList<CartItem> GetItems()
  {
    return Items;
  }

}