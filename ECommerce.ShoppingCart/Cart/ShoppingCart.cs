﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ECommerce.Shared;
using ECommerce.ShoppingCart.Exceptions;

namespace ECommerce.ShoppingCart
{
  public class ShoppingCart : IShoppingCart
  {
    private IDeliveryCostCalculator _deliveryCostCalculator;
    private IList<CartItem> _items;
    private IList<CartDiscount> _discounts;
    private string _buyerId;
    private Coupon _coupon;
    private IList<Campaign> _campaigns;
    public ShoppingCart(IDeliveryCostCalculator deliveryCostCalculator)
    {
      _deliveryCostCalculator = deliveryCostCalculator;
      _items = new List<CartItem>();
      _discounts = new List<CartDiscount>();
      _campaigns = new List<Campaign>();
    }

    public IList<CartDiscount> GetDiscounts()
    {
      return _discounts;
    }
    public IList<CartItem> GetItems()
    {
      return _items;
    }
    public void SetBuyerId(string buyerId)
    {
      _buyerId = buyerId;
    }
    public void RemoveItem(Guid productId, int quantity)
    {
      if (productId == null || productId == Guid.Empty)
        throw new ArgumentNullException(nameof(productId));
      if (quantity <= 0)
        throw new ArgumentOutOfRangeException("Quantity cannot be less than 1");

      var relatedItem = _items.FirstOrDefault(x => x.ProductId == productId);

      if (relatedItem == null)
        throw new NotFoundException("Product not found in cart!");

      if (relatedItem.Quantity == quantity)
      {
        _items.Remove(relatedItem);
      }
      else
      {
        relatedItem.Quantity -= quantity;
      }

      ApplyDiscounts();
    }

    public void AddItem(Guid productId, string productName,
                        Guid categoryId, string categoryName,
                        Money unitPrice, int quantity)
    {
      if (productId == null || productId == Guid.Empty)
        throw new ArgumentNullException(nameof(productId));
      if (quantity <= 0)
        throw new ArgumentOutOfRangeException("Quantity cannot be less than 1");
      if (categoryId == null || categoryId == Guid.Empty)
        throw new ArgumentNullException("Product cannot be null");
      if (unitPrice == null)
        throw new ArgumentNullException("UnitPrice cannot be null");

      var relatedItem = _items.FirstOrDefault(x => x.ProductId == productId);

      if (relatedItem != null)
      {
        relatedItem.Quantity += quantity;
      }
      else
      {
        _items.Add(new CartItem()
        {
          CategoryName = categoryName,
          CategoryId = categoryId,
          ProductId = productId,
          ProductName = productName,
          Quantity = quantity,
          UnitPrice = unitPrice
        });
      }

      ApplyDiscounts();
    }
    public void ApplyCoupon(Coupon coupon)
    {
      _discounts.ToList().RemoveAll(x => x.DiscountMethod == DiscountMethod.Coupon);

      if (coupon != null)
      {
        _coupon = coupon;
        var cartModel = new CartModel(_items, _discounts);
        var couponDiscount = coupon.CalculateDiscount(cartModel);

        if (couponDiscount > 0)
        {
          AddDiscount(couponDiscount, coupon.Description, DiscountMethod.Coupon);
        }
      }
    }
    public void ApplyCampaigns(params Campaign[] campaigns)
    {
      ClearDiscounts();

      _campaigns = campaigns.ToList();

      var cartModel = new CartModel(_items, _discounts);
      var categoryDiscounts = new Dictionary<Guid, CartDiscount>();

      foreach (var campaign in campaigns)
      {
        var campaignDiscount = campaign.CalculateDiscount(cartModel);
        var categoryId = campaign.GetCategoryId();

        if (campaignDiscount > 0)
        {
          if (categoryDiscounts.ContainsKey(categoryId))
          {
            if (categoryDiscounts[categoryId].Amount < campaignDiscount)
            {
              categoryDiscounts[categoryId].Amount = campaignDiscount;
              categoryDiscounts[categoryId].Description = campaign.Description;
            }
          }
          else
          {
            categoryDiscounts.Add(categoryId, new CartDiscount() { Amount = campaignDiscount, Description = campaign.Description, DiscountMethod = DiscountMethod.Campaign });
          }
        }
      }

      foreach (var discount in categoryDiscounts)
      {
        var discountValue = discount.Value;
        AddDiscount(discountValue.Amount, discountValue.Description, discountValue.DiscountMethod);
      }
    }

    public decimal GetCampaignDiscount()
    {
      return _discounts.Where(x => x.DiscountMethod == DiscountMethod.Campaign).Sum(x => x.Amount);
    }
    public decimal GetCouponDiscount()
    {
      var discount = _discounts.FirstOrDefault(x => x.DiscountMethod == DiscountMethod.Coupon);
      if (discount == null)
        return 0;

      return discount.Amount;
    }
    public decimal GetTotalAmount()
    {
      decimal totalAmount = 0;
      foreach (var item in _items)
      {
        totalAmount += item.Quantity * item.UnitPrice.Amount;
      }

      return totalAmount;
    }

    public decimal GetTotalDiscounts()
    {
      return GetCampaignDiscount() + GetCouponDiscount();
    }
    public decimal GetTotalAmountAfterDiscounts()
    {
      return GetTotalAmount() - GetCampaignDiscount() - GetCouponDiscount();
    }
    public decimal GetDeliveryCost()
    {
      var numberOfDistinctCategories = _items.GroupBy(x => x.CategoryId).Count();
      var productsCount = _items.GroupBy(x => x.ProductId).Count();

      return _deliveryCostCalculator.Calculate(numberOfDistinctCategories, productsCount);
    }

    public override string ToString()
    {
      var builder = new StringBuilder();
      builder.AppendLine();
      builder.AppendLine($"-----Items in Cart-----");
      builder.AppendLine();

      var categoryGroups = _items.GroupBy(x => x.CategoryName);
      foreach (var group in categoryGroups)
      {
        var categoryName = group.Key;
        var products = group.ToList();

        builder.AppendLine();
        builder.AppendLine($"Category                 : {categoryName}");
        builder.AppendLine();

        foreach (var p in products)
        {
          builder.AppendLine($"Name: {p.ProductName} | UnitPrice: {p.UnitPrice} | Quantity: {p.Quantity}");
        }
      }

      builder.AppendLine();
      builder.AppendLine($"-----Discounts-----");
      builder.AppendLine();
      foreach (var discount in _discounts)
      {
        builder.AppendLine($"Description              : {discount.Description} | Method: {discount.DiscountMethod.ToString()} | Amount: {discount.Amount}");
      }

      builder.AppendLine();
      builder.AppendLine($"-----General Information-----");
      builder.AppendLine();
      builder.AppendLine($"TotalAmount              : {GetTotalAmount()}");
      builder.AppendLine($"DeliveryCost             : {GetDeliveryCost()}");
      builder.AppendLine($"CampaignDiscount         : {GetCampaignDiscount()}");
      builder.AppendLine($"CouponDiscount           : {GetCouponDiscount()}");
      builder.AppendLine($"TotalAmountAfterDiscounts: {GetTotalAmountAfterDiscounts()}");

      return builder.ToString();
    }
    private void ApplyDiscounts()
    {
      ClearDiscounts();
      ApplyCampaigns(_campaigns.ToArray());
      ApplyCoupon(_coupon);
    }
    private void ClearDiscounts()
    {
      _discounts = new List<CartDiscount>();
    }
    private void AddDiscount(decimal amount, string description, DiscountMethod discountMethod)
    {
      _discounts.Add(new CartDiscount()
      {
        Amount = amount,
        Description = description,
        DiscountMethod = discountMethod
      });
    }
  }

}
