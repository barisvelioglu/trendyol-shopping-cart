using System;
using ECommerce.Shared;

namespace ECommerce.ShoppingCart
{
  public class Product
  {
    public Product(string title, Category category, Money unitPrice)
    {
      if (string.IsNullOrEmpty(title))
        throw new ArgumentNullException(nameof(title));

      if (category == null)
        throw new ArgumentNullException(nameof(category));

      if (unitPrice == null)
        throw new ArgumentNullException(nameof(unitPrice));

      Title = title;
      Category = category;
      UnitPrice = unitPrice;
      Id = Guid.NewGuid();
    }
    public Guid Id { get; set; }
    public string Title { get; set; }
    public Category Category { get; set; }
    public Money UnitPrice { get; set; }
  }

}
