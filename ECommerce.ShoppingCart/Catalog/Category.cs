using System;

namespace ECommerce.ShoppingCart
{
  public class Category
  {
    public Category(string name)
    {
      if (string.IsNullOrEmpty(name))
        throw new ArgumentNullException(nameof(name));

      Name = name;
      Id = Guid.NewGuid();
    }

    public Category(string name, Category parentCategory) : this(name)
    {
      if (parentCategory == null)
        throw new ArgumentNullException(nameof(parentCategory));

      ParentCategory = parentCategory;
    }

    public Guid Id { get; set; }
    public string Name { get; set; }
    public Category ParentCategory { get; set; }
  }
}