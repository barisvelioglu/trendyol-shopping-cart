public class FixedDeliveryCostCalculator : IDeliveryCostCalculator
{
  private const decimal Fixed_Cost = 2.99m;
  private decimal _costPerDelivery;
  private decimal _costPerProduct;

  public FixedDeliveryCostCalculator(decimal costPerDelivery, decimal costPerProduct)
  {
    _costPerDelivery = costPerDelivery;
    _costPerProduct = costPerProduct;
  }

  public decimal Calculate(int numberOfDeliveries, int numberOfProducts)
  {
    return numberOfDeliveries * _costPerDelivery + numberOfProducts * _costPerProduct + Fixed_Cost;
  }
}



