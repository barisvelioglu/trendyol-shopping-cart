public interface IDeliveryCostCalculator
{
  decimal Calculate(int numberOfDeliveries, int numberOfProducts);
}



