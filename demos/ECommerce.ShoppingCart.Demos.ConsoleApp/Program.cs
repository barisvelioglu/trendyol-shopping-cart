﻿using System;
using System.Collections.Generic;
using ECommerce.Shared;

namespace ECommerce.ShoppingCart.Demos.ConsoleApp
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Hello Trendyol!");

      var computers = new Category("Computers");
      var printers = new Category("Printers", computers);
      var storage = new Category("Storage", computers);

      var printer1 = new Product("Printer 1", printers, new Money("TL", 1500m));
      var printer2 = new Product("Printer 2", printers, new Money("TL", 1600m));
      var printer3 = new Product("Printer 3", printers, new Money("TL", 5500m));

      var storage1 = new Product("Storage 1", storage, new Money("TL", 500m));
      var storage2 = new Product("Storage 2", storage, new Money("TL", 800m));

      var fashion = new Category("Fashion");
      var clothing = new Category("Clothing", fashion);
      var shoes = new Category("Shoes", fashion);

      var clothing1 = new Product("Clothing 1", clothing, new Money("TL", 70m));
      var clothing2 = new Product("Clothing 2", clothing, new Money("TL", 95m));
      var clothing3 = new Product("Clothing 3", clothing, new Money("TL", 135m));

      var shoe1 = new Product("Shoe 1", shoes, new Money("TL", 350m));
      var shoe2 = new Product("Shoe 2", shoes, new Money("TL", 450m));

      var rule1 = new InCategoryAndQuantitySufficientRule("If printers quantity is at least 5 in shopping cart!",
                                  printers.Id,
                                  5);

      var rule2 = new InCategoryAndQuantitySufficientRule("If storage equipments' quantity is at least 2 in shopping cart!",
                                  storage.Id,
                                  2);

      var rule3 = new MinimumAmountRule("If minimum amount is 1000!", 1000);

      var campaign1 = new Campaign(rule1, 50, DiscountType.Rate, "buy 5 pay less");
      var campaign2 = new Campaign(rule2, 20, DiscountType.Amount, "buy 2 pay less");

      var coupon1 = new Coupon(new List<IRule>() { rule3 }, "ASDQWE123", 150, DiscountType.Amount, "Special Mega Coupon");

      var deliveryCostCalculator = new FixedDeliveryCostCalculator(10, 20);
      var shoppingCart = new ShoppingCart(deliveryCostCalculator);

      shoppingCart.AddItem(printer1.Id, printer1.Title, printer1.Category.Id, printer1.Category.Name, printer1.UnitPrice, 10);
      shoppingCart.AddItem(printer1.Id, printer1.Title, printer1.Category.Id, printer1.Category.Name, printer1.UnitPrice, 10);
      shoppingCart.AddItem(storage1.Id, storage1.Title, storage1.Category.Id, storage1.Category.Name, storage1.UnitPrice, 2);

      shoppingCart.ApplyCampaigns(campaign1, campaign2);
      shoppingCart.ApplyCoupon(coupon1);


      System.Console.WriteLine(shoppingCart.ToString());


    }
  }
}
