.NET Core 3.1 projesidir. Test kütüphanesi olarak XUnit tercih ettim. 

Aşağıdaki komutlar ile projeyi derleyip çalıştırabilirsiniz.

dotnet build
dotnet run --project demos/ECommerce.ShoppingCart.Demos.ConsoleApp/ECommerce.ShoppingCart.Demos.ConsoleApp.csproj

NOTLAR

1- Kategorilerde kampanyaların hiyerarşik olarak ele alınmasına yer vermedim.

OUTPUT

Hello Trendyol!

-----Items in Cart-----


Category                 : Printers

Name: Printer 1 | UnitPrice: ECommerce.Shared.Money | Quantity: 20

Category                 : Storage

Name: Storage 1 | UnitPrice: ECommerce.Shared.Money | Quantity: 2

-----Discounts-----

Description              : buy 5 pay less | Method: Campaign | Amount: 50
Description              : buy 2 pay less | Method: Campaign | Amount: 20
Description              : Special Mega Coupon | Method: Coupon | Amount: 150

-----General Information-----

TotalAmount              : 31000
DeliveryCost             : 62.99
CampaignDiscount         : 50
CouponDiscount           : 150
TotalAmountAfterDiscounts: 30800

➜  Trendyol.ShoppingCartCase git:(master) ✗ dotnet run --project demos/ECommerce.ShoppingCart.Demos.ConsoleApp/ECommerce.ShoppingCart.Demos.ConsoleApp.csproj
Hello Trendyol!

-----Items in Cart-----


Category                 : Printers

Name: Printer 1 | UnitPrice: ECommerce.Shared.Money | Quantity: 20

Category                 : Storage

Name: Storage 1 | UnitPrice: ECommerce.Shared.Money | Quantity: 2

-----Discounts-----

Description              : buy 5 pay less | Method: Campaign | Amount: 50
Description              : buy 2 pay less | Method: Campaign | Amount: 20
Description              : Special Mega Coupon | Method: Coupon | Amount: 150

-----General Information-----

TotalAmount              : 31000
DeliveryCost             : 62.99
CampaignDiscount         : 70
CouponDiscount           : 150
TotalAmountAfterDiscounts: 30780