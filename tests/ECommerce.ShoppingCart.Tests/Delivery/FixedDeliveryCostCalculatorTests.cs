using Xunit;

namespace ECommerce.ShoppingCart.Tests.Delivery
{
  public class FixedDeliveryCostCalculatorTests
  {
    [Theory]
    [InlineData(10, 10, 20, 10, 302.99)]
    [InlineData(0, 0, 20, 10, 2.99)]
    public void Calculate_ValidDeliveryCost_ReturnsResult(decimal costPerDelivery, decimal costPerProduct, int numberOfDeliveries, int numberOfProducts, decimal expectedValue)
    {
      var calculator = new FixedDeliveryCostCalculator(costPerDelivery, costPerProduct);
      Assert.Equal(calculator.Calculate(numberOfDeliveries, numberOfProducts), expectedValue);
    }
  }
}