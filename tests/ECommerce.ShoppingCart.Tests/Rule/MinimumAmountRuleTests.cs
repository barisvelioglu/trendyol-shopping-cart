using System;
using System.Collections.Generic;
using ECommerce.Shared;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Rule
{
  public class MinimumAmountRuleTests
  {
    private readonly Product _product;
    private readonly Category _category;
    public MinimumAmountRuleTests()
    {
      _category = new Category("TestCategory");
      _product = new Product("TestProduct", _category, new Money("TL", 50));
    }
    [Fact]
    public void Initialize_NegativeAmount_ThrowsArgumentOutOfRangeException()
    {
      Assert.Throws<ArgumentOutOfRangeException>(() => new MinimumAmountRule("TestDescription", -100));
    }

    [Fact]
    public void IsMatched_MatchingRule_ReturnsTrue()
    {
      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = _product.Category.Id;
      mockCartItem.CategoryName = _product.Category.Name;
      mockCartItem.ProductId = _product.Id;
      mockCartItem.ProductName = _product.Title;
      mockCartItem.UnitPrice = _product.UnitPrice;
      mockCartItem.Quantity = 100;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      IRule rule = new MinimumAmountRule("TestDescription", 1000);
      Assert.True(rule.IsMatched(mockCartModel));
    }

    [Fact]
    public void IsMatched_NotMatchingRule_ReturnsFalse()
    {
      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = _product.Category.Id;
      mockCartItem.CategoryName = _product.Category.Name;
      mockCartItem.ProductId = _product.Id;
      mockCartItem.ProductName = _product.Title;
      mockCartItem.UnitPrice = _product.UnitPrice;
      mockCartItem.Quantity = 1;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      IRule rule = new MinimumAmountRule("TestDescription", 500);
      Assert.False(rule.IsMatched(mockCartModel));
    }

  }
}