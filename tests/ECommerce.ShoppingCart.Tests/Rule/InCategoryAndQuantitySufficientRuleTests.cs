using System;
using System.Collections.Generic;
using ECommerce.Shared;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Rule
{
  public class InCategoryAndQuantitySufficientRuleTests
  {
    private readonly Product _product1;
    private readonly Product _product2;
    private readonly Category _category1;
    private readonly Category _category2;
    public InCategoryAndQuantitySufficientRuleTests()
    {
      _category1 = new Category("TestCategory1");
      _category2 = new Category("TestCategory2");
      _product1 = new Product("TestProduct", _category1, new Money("TL", 50));
      _product2 = new Product("TestProduct", _category2, new Money("TL", 50));

    }

    [Fact]
    public void Initialize_NullCategory_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new InCategoryAndQuantitySufficientRule("TestDescription", Guid.Empty, 10));
    }

    [Fact]
    public void Initialize_QuantityIsLessThanOne_ThrowsArgumentOutOfRangeException()
    {
      Assert.Throws<ArgumentOutOfRangeException>(() => new InCategoryAndQuantitySufficientRule("TestDescription", _category1.Id, 0));
    }

    [Fact]
    public void IsMatched_MatchingRule_ReturnsTrue()
    {
      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = _product1.Category.Id;
      mockCartItem.CategoryName = _product1.Category.Name;
      mockCartItem.ProductId = _product1.Id;
      mockCartItem.ProductName = _product1.Title;
      mockCartItem.UnitPrice = _product1.UnitPrice;
      mockCartItem.Quantity = 100;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      IRule rule = new InCategoryAndQuantitySufficientRule("TestDescription", _category1.Id, 10);
      Assert.True(rule.IsMatched(mockCartModel));
    }

    [Fact]
    public void IsMatched_SameCategoryButQuantityIsNotEnough_ReturnsFalse()
    {
      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = _product1.Category.Id;
      mockCartItem.CategoryName = _product1.Category.Name;
      mockCartItem.ProductId = _product1.Id;
      mockCartItem.ProductName = _product1.Title;
      mockCartItem.UnitPrice = _product1.UnitPrice;
      mockCartItem.Quantity = 9;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      IRule rule = new InCategoryAndQuantitySufficientRule("TestDescription", _category1.Id, 10);
      Assert.False(rule.IsMatched(mockCartModel));
    }

    [Fact]
    public void IsMatched_DifferentCategoryButQuantityIsEnough_ReturnsFalse()
    {
      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = _product1.Category.Id;
      mockCartItem.CategoryName = _product1.Category.Name;
      mockCartItem.ProductId = _product1.Id;
      mockCartItem.ProductName = _product1.Title;
      mockCartItem.UnitPrice = _product1.UnitPrice;
      mockCartItem.Quantity = 20;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      IRule rule = new InCategoryAndQuantitySufficientRule("TestDescription", _category2.Id, 10);
      Assert.False(rule.IsMatched(mockCartModel));
    }

    [Fact]
    public void IsMatched_MultipleCategoriesButQuantityIsNotEnough_ReturnsFalse()
    {
      var mockCartItem1 = new CartItem();
      mockCartItem1.CategoryId = _product1.Category.Id;
      mockCartItem1.CategoryName = _product1.Category.Name;
      mockCartItem1.ProductId = _product1.Id;
      mockCartItem1.ProductName = _product1.Title;
      mockCartItem1.UnitPrice = _product1.UnitPrice;
      mockCartItem1.Quantity = 20;

      var mockCartItem2 = new CartItem();
      mockCartItem2.CategoryId = _product2.Category.Id;
      mockCartItem2.CategoryName = _product2.Category.Name;
      mockCartItem2.ProductId = _product2.Id;
      mockCartItem2.ProductName = _product2.Title;
      mockCartItem2.UnitPrice = _product2.UnitPrice;
      mockCartItem2.Quantity = 30;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem1, mockCartItem2 }, new List<CartDiscount>());

      IRule rule = new InCategoryAndQuantitySufficientRule("TestDescription", _category2.Id, 40);
      Assert.False(rule.IsMatched(mockCartModel));
    }
  }
}