using System;
using ECommerce.Shared;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Catalog
{
  public class ProductTests
  {
    Category _testCategory;
    Money _testPrice;

    public ProductTests()
    {
      _testCategory = new Category("TestCategory");
      _testPrice = new Money("TL", 61);
    }

    [Fact]
    public void Initialize_EmptyTitle_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Product("", _testCategory, _testPrice));
    }

    [Fact]
    public void Initialize_NullCategory_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Product("TestProduct", null, _testPrice));
    }

    [Fact]
    public void Initialize_NullPrice_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Product("TestProduct", _testCategory, null));
    }
  }
}