using System;
using Moq;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Catalog
{
  public class CategoryTests
  {
    [Fact]
    public void Initialize_EmptyCategoryName_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Category(""));
    }

    [Fact]
    public void Initialize_NullParentCategory_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Category("TestCategory", null));
    }

  }
}