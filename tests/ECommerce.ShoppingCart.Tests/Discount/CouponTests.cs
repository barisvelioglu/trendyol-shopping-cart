using System;
using System.Collections.Generic;
using ECommerce.Shared;
using Moq;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Discount
{
  public class CouponTests
  {
    private readonly string _validCouponCode = "ABCDEFG123";
    public CouponTests()
    {

    }
    [Fact]
    public void Initialize_NullCode_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Coupon(null, 100, DiscountType.Amount, "TestDescription"));
    }

    [Fact]
    public void Initialize_EmptyCode_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Coupon("", 100, DiscountType.Amount, "TestDescription"));
    }

    [Fact]
    public void Initialize_InvalidDiscount_ThrowsArgumentOutOfRangeException()
    {
      var mockRule = new Mock<IRule>();
      Assert.Throws<ArgumentOutOfRangeException>(() => new Coupon(new List<IRule>() { mockRule.Object }, _validCouponCode, 0, DiscountType.Amount, "TestDescription"));
    }

    [Fact]
    public void Calculate_RulesDoesntMatch_AlwaysReturnsZero()
    {
      var mockCategory = new Category("TestCategory");
      var mockProduct = new Product("TestProduct", mockCategory, new Money("TL", 100));

      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = mockProduct.Category.Id;
      mockCartItem.CategoryName = mockProduct.Category.Name;
      mockCartItem.ProductId = mockProduct.Id;
      mockCartItem.ProductName = mockProduct.Title;
      mockCartItem.UnitPrice = mockProduct.UnitPrice;
      mockCartItem.Quantity = 10;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      var mockRule = new Mock<IRule>();
      mockRule.Setup(m => m.IsMatched(It.IsAny<CartModel>())).Returns(false);

      var campaign = new Coupon(new List<IRule>() { mockRule.Object }, _validCouponCode, 20, DiscountType.Amount, "TestDescription");
      Assert.Equal(campaign.CalculateDiscount(mockCartModel), 0);
    }

    [Fact]
    public void Calculate_AllRulesMatches_ReturnsValidDiscount()
    {
      var mockCategory = new Category("TestCategory");
      var mockProduct = new Product("TestProduct", mockCategory, new Money("TL", 100));

      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = mockProduct.Category.Id;
      mockCartItem.CategoryName = mockProduct.Category.Name;
      mockCartItem.ProductId = mockProduct.Id;
      mockCartItem.ProductName = mockProduct.Title;
      mockCartItem.UnitPrice = mockProduct.UnitPrice;
      mockCartItem.Quantity = 10;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      var mockRule1 = new Mock<IRule>();
      mockRule1.Setup(m => m.IsMatched(It.IsAny<CartModel>())).Returns(true);

      var mockRule2 = new Mock<IRule>();
      mockRule2.Setup(m => m.IsMatched(It.IsAny<CartModel>())).Returns(true);

      var campaign = new Coupon(new List<IRule>() { mockRule1.Object, mockRule2.Object }, _validCouponCode, 20, DiscountType.Amount, "TestDescription");

      Assert.True(campaign.CalculateDiscount(mockCartModel) > 0, "The discount greater than 0");

    }

    [Fact]
    public void Calculate_AnyRulesFail_AlwaysReturnsZero()
    {
      var mockCategory = new Category("TestCategory");
      var mockProduct = new Product("TestProduct", mockCategory, new Money("TL", 100));

      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = mockProduct.Category.Id;
      mockCartItem.CategoryName = mockProduct.Category.Name;
      mockCartItem.ProductId = mockProduct.Id;
      mockCartItem.ProductName = mockProduct.Title;
      mockCartItem.UnitPrice = mockProduct.UnitPrice;
      mockCartItem.Quantity = 10;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      var mockRule1 = new Mock<IRule>();
      mockRule1.Setup(m => m.IsMatched(It.IsAny<CartModel>())).Returns(true);

      var mockRule2 = new Mock<IRule>();
      mockRule2.Setup(m => m.IsMatched(It.IsAny<CartModel>())).Returns(false);

      var mockRule3 = new Mock<IRule>();
      mockRule3.Setup(m => m.IsMatched(It.IsAny<CartModel>())).Returns(true);

      var campaign = new Coupon(new List<IRule>() { mockRule1.Object, mockRule2.Object, mockRule3.Object }, _validCouponCode, 20, DiscountType.Amount, "TestDescription");

      Assert.Equal(campaign.CalculateDiscount(mockCartModel), 0);

    }
  }

}