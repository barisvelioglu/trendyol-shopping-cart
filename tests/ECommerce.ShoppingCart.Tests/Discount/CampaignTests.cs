using System;
using System.Collections.Generic;
using ECommerce.Shared;
using Moq;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Discount
{
  public class CampaignTests
  {
    public CampaignTests()
    {
    }

    [Fact]
    public void Initialize_NullRule_ThrowsArgumentNullException()
    {
      Assert.Throws<ArgumentNullException>(() => new Campaign(null, 100, DiscountType.Amount, "TestDescription"));
    }


    [Fact]
    public void Initialize_InvalidDiscount_ThrowsArgumentOutOfRangeException()
    {
      var mockRule = new Mock<ICategoryRule>();
      Assert.Throws<ArgumentOutOfRangeException>(() => new Campaign(mockRule.Object, 0, DiscountType.Amount, "TestDescription"));
    }



    [Fact]
    public void Calculate_RulesDoesntMatch_AlwaysReturnsZero()
    {
      var mockCategory = new Category("TestCategory");
      var mockProduct = new Product("TestProduct", mockCategory, new Money("TL", 100));

      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = mockProduct.Category.Id;
      mockCartItem.CategoryName = mockProduct.Category.Name;
      mockCartItem.ProductId = mockProduct.Id;
      mockCartItem.ProductName = mockProduct.Title;
      mockCartItem.UnitPrice = mockProduct.UnitPrice;
      mockCartItem.Quantity = 10;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      var mockRule = new Mock<ICategoryRule>();
      mockRule.Setup(m => m.IsMatched(It.IsAny<CartModel>())).Returns(false);

      var campaign = new Campaign(mockRule.Object, 20, DiscountType.Amount, "TestDescription");
      Assert.Equal(campaign.CalculateDiscount(mockCartModel), 0);
    }

    [Fact]
    public void Calculate_RuleMatches_ReturnsValidDiscount()
    {
      var mockCategory = new Category("TestCategory");
      var mockProduct = new Product("TestProduct", mockCategory, new Money("TL", 100));

      var mockCartItem = new CartItem();
      mockCartItem.CategoryId = mockProduct.Category.Id;
      mockCartItem.CategoryName = mockProduct.Category.Name;
      mockCartItem.ProductId = mockProduct.Id;
      mockCartItem.ProductName = mockProduct.Title;
      mockCartItem.UnitPrice = mockProduct.UnitPrice;
      mockCartItem.Quantity = 10;

      var mockCartModel = new CartModel(new List<CartItem>() { mockCartItem }, new List<CartDiscount>());

      var rule = new InCategoryAndQuantitySufficientRule("If storage equipments' quantity is at least 2 in shopping cart!",
                                                          mockProduct.Category.Id,
                                                          2);

      var campaign = new Campaign(rule, 20, DiscountType.Amount, "TestDescription");

      Assert.True(campaign.CalculateDiscount(mockCartModel) > 0, "The discount greater than 0");

    }

  }
}