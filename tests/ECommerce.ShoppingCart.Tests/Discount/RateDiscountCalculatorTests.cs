using System;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Discount
{
  public class RateDiscountCalculatorTests
  {
    IDiscountCalculator calculator;
    public RateDiscountCalculatorTests()
    {
      calculator = new RateDiscountCalculator();
    }

    [Theory]
    [InlineData(100, 101)]
    [InlineData(100, 0)]
    [InlineData(100, -10)]
    public void Calculate_DiscountRateNotBetween1And100_ThrowsArgumentOutOfRangeException(decimal totalAmount, decimal discountRate)
    {
      Assert.Throws<ArgumentOutOfRangeException>(() => calculator.Calculate(totalAmount, discountRate));
    }

    [Theory]
    [InlineData(1500, 100)]
    [InlineData(2000, 100)]
    public void Calculate_DiscountRate100_AlwaysReturnsTotalAmount(decimal totalAmount, decimal discountAmount)
    {
      Assert.Equal(totalAmount, calculator.Calculate(totalAmount, discountAmount));
    }

    [Theory]
    [InlineData(100, 80, 80)]
    [InlineData(500, 50, 250)]
    [InlineData(210, 10, 21)]
    [InlineData(350, 20, 70)]
    public void Calculate_DiscountLessThanTotalAmount_AlwaysReturnsRemainder(decimal totalAmount, decimal discountAmount, decimal expectedValue)
    {
      Assert.Equal(calculator.Calculate(totalAmount, discountAmount), expectedValue);
    }

  }
}