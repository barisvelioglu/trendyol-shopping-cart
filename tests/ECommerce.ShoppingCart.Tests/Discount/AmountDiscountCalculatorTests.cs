using System;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Discount
{
  public class AmountDiscountCalculatorTests
  {
    IDiscountCalculator calculator;
    public AmountDiscountCalculatorTests()
    {
      calculator = new AmountDiscountCalculator();
    }
    [Fact]
    public void Calculate_NegativeDiscountAmount_ThrowsArgumentException()
    {
      Assert.Throws<ArgumentException>(() => calculator.Calculate(100, -100));
    }

    [Fact]
    public void Calculate_DiscountGreaterThanTotalAmount_AlwaysReturnsZero()
    {
      Assert.Equal(100, calculator.Calculate(100, 150));
    }

    [Theory]
    [InlineData(100, 75, 75)]
    [InlineData(100, 39, 39)]
    public void Calculate_DiscountLessThanTotalAmount_AlwaysReturnsRemainder(decimal totalAmount, decimal discountAmount, decimal expectedValue)
    {
      Assert.Equal(expectedValue, calculator.Calculate(totalAmount, discountAmount));
    }

  }
}