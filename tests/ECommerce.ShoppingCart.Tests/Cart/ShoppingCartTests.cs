using System;
using System.Collections.Generic;
using System.Linq;
using ECommerce.Shared;
using Moq;
using Xunit;

namespace ECommerce.ShoppingCart.Tests.Cart
{
  public class ShoppingCartTests
  {
    private readonly Category _category1;
    private readonly Category _category2;
    private readonly Category _category3;
    private readonly Product _product1;
    private readonly Product _product2;
    private readonly Product _product3;

    private readonly Product _product10;
    private readonly Product _product20;
    private readonly Product _product30;
    private readonly Product _product40;

    private readonly Category _category10;
    private readonly Category _category20;
    private readonly Category _category30;
    private readonly IRule _rule1;
    private readonly IRule _rule2;
    private readonly IRule _rule3;
    private readonly Campaign _campaign1;
    private readonly Campaign _campaign2;
    private readonly Coupon _coupon1;
    private readonly Coupon _coupon2;
    private readonly Coupon _coupon3;
    public ShoppingCartTests()
    {
      _category1 = new Category("Computers");
      _category2 = new Category("Printers", _category1);
      _category3 = new Category("Storage", _category1);

      _product1 = new Product("Printer 1", _category2, new Money("TL", 1500m));
      _product2 = new Product("Printer 2", _category2, new Money("TL", 1600m));
      _product3 = new Product("Printer 3", _category2, new Money("TL", 5500m));

      _product10 = new Product("Storage 1", _category3, new Money("TL", 500));
      _product20 = new Product("Storage 2", _category3, new Money("TL", 200));

      _category10 = new Category("Fashion");
      _category20 = new Category("Clothing", _category10);
      _category30 = new Category("Shoes", _category10);

      _product30 = new Product("Shoe 1", _category30, new Money("TL", 350m));
      _product40 = new Product("Shoe 2", _category30, new Money("TL", 450m));

      _rule1 = new InCategoryAndQuantitySufficientRule("If printers quantity is at least 5 in shopping cart!",
                                                          _category2.Id,
                                                          5);

      _rule2 = new InCategoryAndQuantitySufficientRule("If storage equipments' quantity is at least 2 in shopping cart!",
                                                          _category3.Id,
                                                          2);

      _rule3 = new MinimumAmountRule("If cart amount is more than 1000", 1000);

    }

    [Fact]
    public void AddItem_ProductIdIsEmpty_ThrowsArgumentNullException()
    {
      var mockRule = new Mock<IDeliveryCostCalculator>();
      var mockMoney = new Money("TL", 100m);

      var shoppingCart = new ShoppingCart(mockRule.Object);

      Assert.Throws<ArgumentNullException>(() => shoppingCart.AddItem(Guid.Empty, "ProductName", Guid.NewGuid(), "CategoryName", mockMoney, 10));
    }

    [Fact]
    public void AddItem_CategoryIdIsEmpty_ThrowsArgumentNullException()
    {
      var mockRule = new Mock<IDeliveryCostCalculator>();
      var mockMoney = new Money("TL", 100m);

      var shoppingCart = new ShoppingCart(mockRule.Object);

      Assert.Throws<ArgumentNullException>(() => shoppingCart.AddItem(Guid.NewGuid(), "ProductName", Guid.Empty, "CategoryName", mockMoney, 10));
    }

    [Fact]
    public void AddItem_QuantityIsLessThan1_ThrowsArgumentOutOfRangeException()
    {
      var mockRule = new Mock<IDeliveryCostCalculator>();
      var mockMoney = new Money("TL", 100m);

      var shoppingCart = new ShoppingCart(mockRule.Object);

      Assert.Throws<ArgumentOutOfRangeException>(() => shoppingCart.AddItem(Guid.NewGuid(), "ProductName", Guid.NewGuid(), "CategoryName", mockMoney, 0));
    }

    [Fact]
    public void AddItem_UnitPriceIsNull_ThrowsArgumentOutOfRangeException()
    {
      var mockRule = new Mock<IDeliveryCostCalculator>();
      var mockMoney = new Money("TL", 100m);

      var shoppingCart = new ShoppingCart(mockRule.Object);

      Assert.Throws<ArgumentNullException>(() => shoppingCart.AddItem(Guid.NewGuid(), "ProductName", Guid.NewGuid(), "CategoryName", null, 10));
    }

    [Fact]
    public void AddItem_AlreadyExistsItem_DoesntChangeItemCountOnlyIncreasesItemQuantity()
    {
      var mockRule = new Mock<IDeliveryCostCalculator>();
      var mockMoney = new Money("TL", 100m);

      var shoppingCart = new ShoppingCart(mockRule.Object);
      var productId = Guid.NewGuid();
      var categoryId = Guid.NewGuid();

      shoppingCart.AddItem(productId, "ProductName", categoryId, "CategoryName", mockMoney, 10);
      shoppingCart.AddItem(productId, "ProductName", categoryId, "CategoryName", mockMoney, 40);

      Assert.Equal(shoppingCart.GetItems().Count, 1);
      Assert.Equal(shoppingCart.GetItems().Sum(x => x.Quantity), 50);

    }

    [Fact]
    public void AddItem_DifferentItem_IncreasesItemCountAndTotalQuantity()
    {
      var mockRule = new Mock<IDeliveryCostCalculator>();
      var mockMoney = new Money("TL", 100m);

      var shoppingCart = new ShoppingCart(mockRule.Object);
      var categoryId = Guid.NewGuid();

      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 10);
      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 10);

      Assert.Equal(shoppingCart.GetItems().Count, 2);
      Assert.Equal(shoppingCart.GetItems().Sum(x => x.Quantity), 20);
    }

    [Fact]
    public void GetTotalAmount_AddDifferentItems_ReturnsTotalAmount()
    {
      var mockRule = new Mock<IDeliveryCostCalculator>();
      var mockMoney = new Money("TL", 100);

      var shoppingCart = new ShoppingCart(mockRule.Object);
      var categoryId = Guid.NewGuid();

      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 10);
      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 50);

      Assert.Equal(shoppingCart.GetTotalAmount(), 6000);
    }

    [Fact]
    public void ApplyCoupon_ValidCoupon_AddDiscounts()
    {
      var mockMoney = new Money("TL", 100);

      var mockCalculator = new Mock<IDeliveryCostCalculator>();

      var shoppingCart = new ShoppingCart(mockCalculator.Object);
      var categoryId = Guid.NewGuid();

      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 10);
      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 50);

      var coupon = new Coupon(new List<IRule>() { _rule3 }, "ASDQWE125", 10, DiscountType.Rate, "Special Coupon3");

      shoppingCart.ApplyCoupon(coupon);
      Assert.Equal(600, shoppingCart.GetCouponDiscount());
    }

    [Fact]
    public void ApplyCoupon_ValidCouponButRuleDoesntMatch_Returns0AsDiscount()
    {
      var mockMoney = new Money("TL", 10);
      var mockCalculator = new Mock<IDeliveryCostCalculator>();

      var shoppingCart = new ShoppingCart(mockCalculator.Object);
      var categoryId = Guid.NewGuid();

      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 10);
      shoppingCart.AddItem(Guid.NewGuid(), "ProductName", categoryId, "CategoryName", mockMoney, 50);

      var coupon = new Coupon(new List<IRule>() { _rule3 }, "ASDQWE125", 10, DiscountType.Rate, "Special Coupon3");

      shoppingCart.ApplyCoupon(_coupon3);
      Assert.Equal(0, shoppingCart.GetCouponDiscount());
    }

    [Fact]
    public void ApplyCampaigns_ValidOneCampaign_ReturnsDiscounts()
    {
      var mockCalculator = new Mock<IDeliveryCostCalculator>();

      var shoppingCart = new ShoppingCart(mockCalculator.Object);

      shoppingCart.AddItem(_product10.Id, _product10.Title, _product10.Category.Id, _product10.Category.Name, _product10.UnitPrice, 1);
      shoppingCart.AddItem(_product20.Id, _product20.Title, _product20.Category.Id, _product20.Category.Name, _product20.UnitPrice, 1);

      var campaign = new Campaign((ICategoryRule)_rule2, 50, DiscountType.Rate, "Buy at least 2 storage equipment pay %50 less");

      shoppingCart.ApplyCampaigns(campaign);
      Assert.Equal(350, shoppingCart.GetCampaignDiscount());
    }

    [Fact]
    public void ApplyCampaigns_ValidButDoesntMatchAnyRule_Returns0AsDiscount()
    {
      var mockCalculator = new Mock<IDeliveryCostCalculator>();

      var shoppingCart = new ShoppingCart(mockCalculator.Object);

      shoppingCart.AddItem(_product10.Id, _product10.Title, _product10.Category.Id, _product10.Category.Name, _product10.UnitPrice, 1);
      shoppingCart.AddItem(_product30.Id, _product30.Title, _product30.Category.Id, _product30.Category.Name, _product30.UnitPrice, 1);

      var campaign = new Campaign((ICategoryRule)_rule2, 50, DiscountType.Rate, "Buy at least 2 storage equipment pay %50 less");

      shoppingCart.ApplyCampaigns(campaign);
      Assert.Equal(0, shoppingCart.GetCampaignDiscount());
    }

    [Fact]
    public void ApplyCampaigns_MoreThanOneCampaign_ReturnsMuchAffectedCampaignDiscount()
    {
      var mockCalculator = new Mock<IDeliveryCostCalculator>();

      var shoppingCart = new ShoppingCart(mockCalculator.Object);

      shoppingCart.AddItem(_product10.Id, _product10.Title, _product10.Category.Id, _product10.Category.Name, _product10.UnitPrice, 20);
      shoppingCart.AddItem(_product30.Id, _product30.Title, _product30.Category.Id, _product30.Category.Name, _product30.UnitPrice, 10);

      var campaign1 = new Campaign((ICategoryRule)_rule2, 300, DiscountType.Amount, "Buy at least 2 storage equipment pay 300 less");
      var campaign2 = new Campaign((ICategoryRule)_rule2, 400, DiscountType.Amount, "Buy at least 2 storage equipment pay 400 less");

      shoppingCart.ApplyCampaigns(campaign1, campaign2);

      Assert.Equal(400, shoppingCart.GetCampaignDiscount());
    }

    [Fact]
    public void GetTotalDiscounts_ApplyValidCouponAndValidCampaign_AppliesTwoDiscounts()
    {
      var mockCalculator = new Mock<IDeliveryCostCalculator>();
      var shoppingCart = new ShoppingCart(mockCalculator.Object);

      shoppingCart.AddItem(_product10.Id, _product10.Title, _product10.Category.Id, _product10.Category.Name, _product10.UnitPrice, 20);
      shoppingCart.AddItem(_product30.Id, _product30.Title, _product30.Category.Id, _product30.Category.Name, _product30.UnitPrice, 10);

      var campaign1 = new Campaign((ICategoryRule)_rule2, 610, DiscountType.Amount, "Buy at least 2 storage equipment pay 610 less");
      var coupon = new Coupon(new List<IRule>() { _rule3 }, "ASDQWE125", 100, DiscountType.Amount, "Special Coupon3");

      shoppingCart.ApplyCampaigns(campaign1);
      shoppingCart.ApplyCoupon(coupon);

      Assert.Equal(710, shoppingCart.GetTotalDiscounts());
    }

    [Fact]
    public void GetTotalAmountAfterDiscounts_ApplyValidCouponAndValidCampaign_AppliesTwoDiscounts()
    {
      var mockCalculator = new Mock<IDeliveryCostCalculator>();
      var shoppingCart = new ShoppingCart(mockCalculator.Object);

      shoppingCart.AddItem(_product10.Id, _product10.Title, _product10.Category.Id, _product10.Category.Name, _product10.UnitPrice, 20);
      shoppingCart.AddItem(_product30.Id, _product30.Title, _product30.Category.Id, _product30.Category.Name, _product30.UnitPrice, 10);

      var campaign1 = new Campaign((ICategoryRule)_rule2, 400, DiscountType.Amount, "Buy at least 2 storage equipment pay 300 less");

      var coupon = new Coupon(new List<IRule>() { _rule3 }, "ASDQWE125", 100, DiscountType.Amount, "Special Coupon3");

      shoppingCart.ApplyCampaigns(campaign1);
      shoppingCart.ApplyCoupon(coupon);

      Assert.Equal(13000, shoppingCart.GetTotalAmountAfterDiscounts());
    }

    [Fact]
    public void GetDeliveryCost_ValidInputs_ReturnsCost()
    {
      var costCalculator = new FixedDeliveryCostCalculator(100, 10);
      var shoppingCart = new ShoppingCart(costCalculator);

      shoppingCart.AddItem(_product10.Id, _product10.Title, _product10.Category.Id, _product10.Category.Name, _product10.UnitPrice, 20);
      shoppingCart.AddItem(_product30.Id, _product30.Title, _product30.Category.Id, _product30.Category.Name, _product30.UnitPrice, 10);
      shoppingCart.AddItem(_product20.Id, _product20.Title, _product20.Category.Id, _product20.Category.Name, _product20.UnitPrice, 10);

      //number of distinct categories 2
      //number of products not quantity 3
      Assert.Equal(232.99m, shoppingCart.GetDeliveryCost());

    }

    [Fact]
    public void GetDeliveryCost_NoItemInCart_ReturnsFixedValueAsCost()
    {
      var costCalculator = new FixedDeliveryCostCalculator(100, 10);
      var shoppingCart = new ShoppingCart(costCalculator);

      Assert.Equal(2.99m, shoppingCart.GetDeliveryCost());

    }

  }
}